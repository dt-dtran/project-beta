<a name="readme-top"></a>
# CarCar
This app is intended to cover most aspects of a car dealership.  Areas of focus related to inventory, sales, maintenance, and staff.

<div style="text-align: right">(<a href="#inventory"> [ inventory ] </a> <a href="#sales"> [ sales ] </a><a href="#service"> [ service ] </a>)</div>

Team:
* **Diana Tran** - Sales
* **Patrick Dwyer** - Services

# Getting Started

## Built with
* React
* Django
* Bootstrap
* Docker

## Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/dt-dtran/project-beta.git
   ```
2. Docker create database volume
   ```sh
   docker volume create beta-data
   ```
3. Docker build containers and images
   ```sh
   docker-compose build
   ```
4. Run Docker
   ```sh
   docker-compose up
   ```
- View the project in the browser: http://localhost:3000/


# Design
CarCar is split into three domains; Inventory, Sales, and Service. Sales and Service poll data from inventory to populate vehicle data for each individual service.

![Img](./CarCar_Design.png)

## Integration
**Automobile VO is a value object**

**Sales** manages 4 models: Customer, Salesperson, Sale, and AutomobileVO. Sale takes data from all the other models and adds a price field to make a complete record of all sales.
* AutomobileVO takes in the ID and VIN from automobile inventory and is accessed through a poller every 60 seconds.
	* AutomobileVO sold status is set to a default false.
* The sold status is updated via a patch request to ensure both VO and Automobile aligns.
	* When automobile is sold, status is updates in the Automobile VO AND Inventory Automobile sold status.
	* When a sale is deleted, the automobile sold status will turn to false for both database.
* Existing sales will retain the data of the salesperson and customer for historical purposes.
	* **Please be advise that if a salesperson or customers is part of a sale, you will not be able to delete the saleperson or customer.**
	* If you really need to, you must delete the sale before doing so.
	* We will consider PUT updates in a future release.

**Service** maintains and collects aspects of a dealership in the domain of repair and maintenance.  Maintenance and repair are handled via appointment.  Appointment will track vehicle vin and technician information.  Canceling or completing appointments is possible, vip information is avaliable on front end.

# Inventory
<a name="inventory"></a>
## Manufacturers - URL info:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


### POST - manufacturer name is a unique value
```
{
  "name": "Chrysler"
}
```
**Create Manufacturer:** Return Value
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
```
### GET
```
{
  "manufacturers": [
    {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Chrysler"
    },
    {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Honda"
    }
  ]
}
```
## Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

### POST - Name, Picture URL, and Manfucaturer id required
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```
**Create Model:** Return Value

```
{
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Chrysler"
    }
}
```
### GET
**List View**
```
{
  "models": [
	{
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
    },
    {
		"href": "/api/models/2/",
		"id": 2,
		"name": "Accord",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Honda_Accord_%28CV3%29_EX_eHEV%2C_2021%2C_front.jpg/280px-Honda_Accord_%28CV3%29_EX_eHEV%2C_2021%2C_front.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Honda"
		}
	}
    ]
}
```

## Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Update sold status on a specific automobile | PATCH | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


### POST - vin must be unique
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC54",
  "model_id": 1
}
```
**Create Automobile:** Return Value
```
{
	"href": "/api/automobiles/1C3CC54/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC54",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
### GET
**List View**
```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC54/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC54",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		},
		{
			"href": "/api/automobiles/HONDAACC1/",
			"id": 2,
			"color": "grey",
			"year": 2023,
			"vin": "HONDAACC1",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "Accord",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Honda_Accord_%28CV3%29_EX_eHEV%2C_2021%2C_front.jpg/280px-Honda_Accord_%28CV3%29_EX_eHEV%2C_2021%2C_front.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Honda"
				}
			},
			"sold": false
		}
	]
}
```

**Specific View by VIN**

To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC54/

Return Value:
```
{
	"href": "/api/automobiles/1C3CC54/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC54",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
**PATCH: update a sold status of a specific automobile**
example URL: http://localhost:8100/api/automobiles/1C3CC54/
```
{
  "sold": false
}
```
<div style="text-align: right">(<a href="#readme-top">Jump to: [ top ] </a> <a href="#inventory"> [ inventory ] <a href="#service"> [ service ] </a>)</div>

# Sales microservice
<a name="sales"></a>

## Customers:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/

### POST
```
{
	"first_name": "Diana",
	"last_name": "Tran",
	"address": "1234 Beta Street, Denver, C0 80000",
	"phone_number": "555-555-5555"
}
```
Return Value of Creating a Customer:
```
{
	"href": "/api/customers/1/",
	"first_name": "Diana",
	"last_name": "Tran",
	"address": "1234 Beta Street, Denver, C0 80000",
	"phone_number": "555-555-5555",
	"id": 1
}
```
### GET
**Listing all Customers:** Return Value
```
{
	"customers": [
		{
			"href": "/api/customers/1/",
			"first_name": "Diana",
			"last_name": "Tran",
			"address": "1234 Beta Street, Denver, C0 80000",
			"phone_number": "555-555-5555",
			"id": 1
		},
		{
			"href": "/api/customers/2/",
			"first_name": "Diana2",
			"last_name": "Tran2",
			"address": "100 Second Stree, Brooklyn, NY 10000",
			"phone_number": "123-456-6790",
			"id": 2
		},
	]
}
```
### DELETE: You will not be able delete a customer if the customer is part of an existing sale.
## Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/employee_id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/employee_id/


### POST - employee_id must be unique and is used to access individual employee data in insomnia
```
{
	"first_name": "Patrick",
	"last_name": "Dwyer",
	"employee_id": "pdwyer1"
}
```
Return Value of creating a salesperson:
```
{
	"salesperson": {
		"first_name": "Patrick",
		"last_name": "Dwyer",
		"employee_id": "pdwyer1"
		"id": 1
	}
}
```
### GET
**List All Salespeople:** Return Value:
```
{
salespeople = [
		{
			"first_name": "Patrick",
			"last_name": "Dwyer",
			"employee_id": "pdwyer1"
			"id": 1
		},
		{
			"first_name": "Patrick2",
			"last_name": "Dwyer2",
			"employee_id": "pdwyer2"
		"id": 2
		}
	]
}
```
**View Specific Salespeople By employee_id**
```
{
	"first_name": "Patrick",
	"last_name": "Dwyer",
	"employee_id": "pdwyer1"
	"id": 1
}
```
### DELETE: You will not be able delete a salesperson if the salesperson is part of an existing sale.

## Sales:
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's salesrecords | GET | http://localhost:8090/api/sales/id/

### POST request
**Create a New Sale:**
```
{
	"automobile_vin": "HONDAACC1",
	"salesperson_id": "pdwyer1",
	"customer_id": 1,
	"price": 21999
}
```
Creating a New Sale: Return Value
```
{
	"href": "/api/sales/1/",
	"automobile": {
		"vin": "HONDAACC1",
		"sold": true,
		"id": 2
	},
	"salesperson": {
		"first_name": "Patrick",
		"last_name": "Dwyer",
		"employee_id": "pdwyer1",
		"id": 1
	},
	"customer": {
		"href": "/api/customers/1/",
		"first_name": "Diana",
		"last_name": "Tran",
		"address": "1234 Beta Street, Denver, C0 80000",
		"phone_number": "555-555-5555"
		"id": 1
	},
	"price": 21999,
	"id": 1
}
```
### GET request
**List All Sales History Records:** Return Value
```
{
	"sales": [
		{
			"href": "/api/sales/1/",
			"automobile": {
				"vin": "HONDAACC1",
				"sold": true,
				"id": 2
			},
			"salesperson": {
				"first_name": "Patrick",
				"last_name": "Dwyer",
				"employee_id": "pdwyer1",
				"id": 1
			},
			"customer": {
				"href": "/api/customers/1/",
				"first_name": "Diana",
				"last_name": "Tran",
				"address": "1234 Beta Street, Denver, C0 80000",
				"phone_number": "555-555-5555"
				"id": 1
			},
			"price": 21999.0,
			"id": 1
		}
	]
}
```
<div style="text-align: right">(<a href="#readme-top">Jump to: [ top ] </a> <a href="#inventory"> [ inventory ] </a> <a href="#sales"> [ sales ] </a>)</div>
# Service microservice
<a name="service"></a>

## Technicians - They are people too... Really!

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/str:employee_id/


### POST employee_id must be unique
```
{
	"first_name": "Last one from insomnia hopefully",
	"last_name": "I'll actually test one more time once im finished",
	"employee_id": "lets gooooo"
}
```
**Server response**
```
{
	"href": "/api/technicians/10/",
	"first_name": "Last one from insomnia hopefully",
	"last_name": "I'll actually test one more time once im finished",
	"employee_id": "lets gooooo"
}
```
### DELETE add employee_id to url with delete request - technicians assigned to an appointment cannot be deleted

## Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Cancel service appointment | PUT | http://localhost:8080/api/appointments/int:id/cancel/
| Finish service appointment | PUT | http://localhost:8080/api/appointments/int:id/finish/

### POST - datetime must be yyyy-mm-ddThh:mm, technician must be employee_id value
status can be set to anything initially, the purpose is to track if appointments are completed or canceled at the end.
```
{
	"vin": "2222222",
	"date_time": "2023-09-10T10:55",
	"technician": "77cle44w889",
	"reason": "Appointment to test date formats.",
	"status": "",
	"customer": "Ghostbusters"
}
```
**Server response**
```
{
	"customer": "Ghostbusters",
	"status": "",
	"date_time": "2023-09-10T10:55",
	"vin": "2222222",
	"reason": "Appointment to test date formats.",
	"technician": {
		"href": "/api/technicians/1/",
		"first_name": "Patrick",
		"last_name": "Dwyer",
		"employee_id": "77cle44w889"
	}
}
```
### For PUT requests simply send empty request to one of the above urls and status will change
<div style="text-align: right">(<a href="#readme-top">Jump to: [ top ] </a> <a href="#inventory"> [ inventory ] </a> <a href="#sales"> [ sales ] </a><a href="#service"> [ service ] </a>)</div>
