import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO
from django.db import IntegrityError

def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        # try:
            AutomobileVO.objects.update_or_create(
                vin = auto["vin"],

            )
        # except IntegrityError:
        #         existing_automobile = AutomobileVO.objects.get(vin=auto["vin"])
        #         existing_sold_status = existing_automobile.sold
        #         if not existing_sold_status:
        #             existing_automobile.sold = auto["sold"]
        #             existing_automobile.save()

def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
