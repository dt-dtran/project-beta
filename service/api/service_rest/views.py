from django.db import IntegrityError
from common.json import ModelEncoder
from .models import Appointment, Technician
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse


class TechnicianModelEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentModelEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer",
        "status",
        "date_time",
        "vin",
        "reason",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianModelEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianModelEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        print(content)

        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianModelEncoder,
                safe=False,
            )
        # 400 error is data thats missing required properties or structure... syntax?
        # integrity errors happen when the database doesnt want you to do something
        # like having a unique technician id an integrity error will be raised when
        # trying to create a technician with an identical id
        except IntegrityError:
            return JsonResponse({"Error": "tech id already exists"}, status=400)


@require_http_methods(["DELETE"])
def api_fire_technician(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=id).delete()
        return JsonResponse({"Delete": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appoinments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentModelEncoder,
            safe=False
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            print(content)
            #
        except json.JSONDecodeError:
            return JsonResponse({"json is bad": "corrupted maybe is the right word?"}, status=400)
        try:
            tech_id = content["technician"]
            technician = Technician.objects.get(employee_id=tech_id)
            content["technician"] = technician
            print(content)
            #
        except KeyError:
            return JsonResponse(
                {"key error: missing technician or bad id": "Technician doent exist or missing"},
                status=400,
            )
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentModelEncoder,
                safe=False
            )
        except IntegrityError:
            return JsonResponse(
                {"Integrety error is duplicate id": "appointment id already exists"},
                status=400
                )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"Delete": count > 0})



@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):

    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"wrong appointment": "appointment doesnt exist"},
            status=400
        )
    return JsonResponse(
        appointment,
        encoder=AppointmentModelEncoder,
        safe=False
    )

@require_http_methods(["PUT"])
def api_finish_appointment(request, id):

    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"wrong appointment": "appointment doesnt exist"},
            status=400
        )
    return JsonResponse(
        appointment,
        encoder=AppointmentModelEncoder,
        safe=False
    )
