from django.db import models
from django.urls import reverse


# def create_unique_id():
#     return str(uuid.uuid4())
# unique universal id,  gives you a unique value 36 characters long,
#  convert it to a string or it will remain as hex
#  slicing teh value is possible ie: str(uuid.uuid4())[:22]
#  would give me a string length of 22 i hope this works


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=420, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=420)
    last_name = models.CharField(max_length=420)
    employee_id = models.CharField(
        max_length=420,
        unique=True,
        # default=create_unique_id,
    )

    def get_api_url(self):
        return reverse(
            "api_fire_technician",
            kwargs={"id": self.id}
        )

    def __str__(self):
        return self.first_name


class Appointment(models.Model):
    choices = (
        ("None", 'no value'),
        ("canceled", 'canceled'),
        ("finished", 'finished')
    )
    customer = models.CharField(max_length=420, null=True)
    date_time = models.DateTimeField(auto_now_add=False)
    reason = models.CharField(max_length=420)
    status = models.CharField(
        max_length=420,
        null=True,
        choices=choices,
    )
    vin = models.CharField(max_length=420,)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT
    )
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return f" {self.customer} {self.reason} {self.status}"

    def get_api_url(self):
        return {
            reverse("api_list_appointments", kwargs={"id": self.vin}),
            reverse("api_cancel_appointment", kwargs={"id": self.vin}),
            reverse("api_finish_appointment", kwargs={"id": self.vin}),
        }
