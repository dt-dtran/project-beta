
function AutosList(props) {

    async function deleteAuto(vin) {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (response.ok) {
          props.setAutosList(newList => newList.filter(auto => auto.vin !== vin));
          props.getAutosList()
        } else {
          console.error("Unable to delete", response.status);
        }
      }

    return(
        <div className="container m-3">
            <div className="row">
            <div className="col-12 col-md-6">
            <h1>Automobiles</h1>
            </div>
            <div className="table-responsive mt-3 ">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {props.autosList.map((auto) => {
                    return(
                        <tr key={auto.vin}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.sold ? 'Yes' : 'No'}</td>
                            <td>
                                <button onClick={() => deleteAuto(auto.vin)} type="button" className="btn btn-outline-secondary">Delete</button>
                            </td>
                        </tr>
                    )})}
                </tbody>
                </table>
        </div>
        </div></div>
    )
}

export default AutosList;
