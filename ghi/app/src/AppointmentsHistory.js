import React, { useEffect, useState } from "react";

function AppointmentsHistory(props) {

    const autos = props.autosList;
    const [searchVIN, setSearchVIN] = useState("")

    function isVIP(appointment) {
        for (let auto of autos) {
            if (auto.vin === appointment.vin) {
                return "VIP";
            }
        }
        return "Is Not";
      };

      const filteredVIN = props.appointmentsList.filter(appointment => {
        return appointment.vin.includes(searchVIN) && (appointment.status === 'finished' || appointment.status === 'canceled');
      })

      useEffect(() => {
        props.getAppointmentList();
      }, []);

      return (
        <div className="container m-3">
        <div className="row">
        <div className="col-12 col-md-10">
          <h1>Service History</h1>
          <input
          type="text"
          placeholder="Search VIN"
          value={searchVIN}
          onChange={event => setSearchVIN(event.target.value)}
          className="form-control mb-3"
          />
          <div className="table-responsive mt-3">
          <table className="table table-hover">
              <thead className="bg-success text-white">
                  <tr>
                      <th>Customer</th>
                      <th>VIP</th>
                      <th>VIN</th>
                      <th>Date and time</th>
                      <th>Technician</th>
                      <th>Reason</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
              {
                  filteredVIN.map(appointment => {
                      return(
                          <tr key={appointment.id}>
                              <td>{appointment.customer}</td>
                              <td>{isVIP(appointment)}</td>
                              <td>{appointment.vin}</td>
                              <td>{new Date(appointment.date_time).toLocaleDateString('en-US', {
                                    year: 'numeric',
                                    month: 'short',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    hour12: true,
                                    })}</td>
                              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                              <td>{appointment.reason}</td>
                              <td>{appointment.status}</td>
                          </tr>
                      )})}
              </tbody>
          </table>
          </div>
        </div>
        </div>
      </div>
      )
}
export default AppointmentsHistory;
