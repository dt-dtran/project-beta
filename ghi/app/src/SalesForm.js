import React, { useState } from 'react'

function SalesForm(props) {

    const [ price, setPrice ] = useState("");
    const [ vin, setVIN ] = useState("")
    const [ salesperson, setSalesperson ] =useState("")
    const [ customer, setCustomer ] = useState("");

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value)
    };

    const handleVINChange = (event) => {
        const value = event.target.value;
        setVIN(value)
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value)
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    };

    const filteredAutos = props.autosList.filter(
        (auto) => !auto.sold
      );

    const updateAutomobileStatus = async (vin) => {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;
        const fetchConfig = {
          method: 'PATCH',
          body: JSON.stringify({ sold: true }),
          headers: {
            'Content-Type': 'application/json'
          }
        };

        const response = await fetch(url, fetchConfig);
        if (!response.ok) {
          console.error("Failed to update automobile status:", response);
        }}

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
            data.automobile_vin = vin;
            data.salesperson_id = salesperson
            data.customer_id = customer;
            data.price = price;

        const url = "http://localhost:8090/api/sales/"
        const fetchConfig ={
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            await updateAutomobileStatus(vin);
            await response.json();
            setPrice("");
            setVIN("");
            setSalesperson("");
            setCustomer("");
            document.getElementById('vin').selectedIndex = 0;
            document.getElementById('salesperson').selectedIndex = 0;
            document.getElementById('customer').selectedIndex = 0;
            props.getAutosList()
            props.getSalesList()

        } else {
            console.error("bad", response)
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale" className="create-sale">
                <div className="form mb-3">
                    <label htmlFor="vin">Automobile VIN</label>
                    <select onChange={handleVINChange} name="vin" id="vin" className="form-select">
                        <option value="">Choose an automobile VIN</option>
                        {
                            filteredAutos.map(auto=> {
                                return (
                                    <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                                )
                            })
                        }
                    </select>
                </div>
                <div className="form mb-3">
                    <label htmlFor="salesperson">Salesperson</label>
                    <select onChange={handleSalespersonChange} name="salesperson" id="salesperson" className="form-select">
                        <option value="">Choose a salesperson </option>
                        {
                            props.salespeopleList.map(salesperson=> {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                            })
                        }
                    </select>
                </div>
                <div className="form mb-3">
                    <label htmlFor="customer">Customer</label>
                    <select onChange={handleCustomerChange} name="customer" id="customer" className="form-select">
                        <option value="">Choose a customer </option>
                        {
                            props.customersList.map(customer=> {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                )
                            })
                        }
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={handlePriceChange}
                        value ={price}
                        placeholder="Price"
                        required
                        type="number"
                        name="price"
                        id="price"
                        className="form-control"
                    />
                    <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
        </div>
    )
}
export default SalesForm;
