import React, { useState } from 'react'

function SalespeopleHistory(props) {

    const [selectedSalesperson, setSelectedSalesperson] = useState({});

    const filteredSales = props.salesList.filter(
        (sale) => sale.salesperson.employee_id === selectedSalesperson
      );

      // Calculate total earnings
    const totalEarnings = filteredSales.reduce(
    (total, sale) => total + sale.price, 0);
    return (
        <div className="container m-3">
            <div className="row">
            <div className="col-12 col-md-6">
                <h1>Sales by employee</h1>
                <select
                value={selectedSalesperson}
                className="form-control border-success mt-3"
                onChange={(event) => setSelectedSalesperson(event.target.value)}>
                    <option value="">Select a salesperson</option>
                    {props.salespeopleList.map((salesperson) => (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                    ))}
                </select>
            </div>
            <div className="col">
                <h1 className="text-success text-right">Total Earnings: {totalEarnings.toLocaleString('en-US', {
                                            style: 'currency',
                                            currency: 'USD'
                                        })}</h1>
            </div>
            </div>
            {selectedSalesperson !== "" && (
                 <div className="table-responsive mt-3 ">
                {filteredSales.length > 0 ? (
                    <table className="table table-striped table-hover">
                        <thead className="bg-success text-white">
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sales Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSales.map((sale)=> {
                                const { salesperson, customer, automobile, price } = sale;
                                return (
                                    <tr key={sale.id} className="border-bottom-0 table-bordered border-success">
                                        <td>{salesperson.first_name} {salesperson.last_name}</td>
                                        <td>{customer.first_name} {customer.last_name}</td>
                                        <td>{automobile.vin}</td>
                                        <td>{price.toLocaleString('en-US', {
                                            style: 'currency',
                                            currency: 'USD'
                                        })}</td>
                                    </tr>
                                )})}
                        </tbody>
                    </table> ) : (
                        <> <p>Select another salesperson.</p>  </>
                    ) }
                    </div>) }
        </div>
    )
}
export default SalespeopleHistory;
