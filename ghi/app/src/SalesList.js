
function SalesList(props) {

    async function deleteSale(id, sale) {
        const url = `http://localhost:8090/api/sales/${id}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (!response.ok) {
            console.error("Cannot delete sale", response);
        } else {
            const automobileUrl = `http://localhost:8100/api/automobiles/${sale.automobile.vin}/`;
            const updateResponse = await fetch(automobileUrl, {
                method: 'PATCH',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  sold: false,
                }),
              });
              if (!updateResponse.ok) {
                console.error("Cannot update automobile sold status", updateResponse);
              } else {
                props.setSalesList(newList => newList.filter(sale=> sale.id !== id));
                props.getSalesList()
                props.getAutosList()
              }
        }
      }
      const totalEarnings = props.salesList.reduce(
        (total, sale) => total + sale.price, 0);

    return (
        <div className="container m-3">
        <div className="row">
            <div className="col-12 col-md-6">
                <h1>Sales</h1>
            </div>
            <div className="col">
                <h1 className="text-success text-right">Total Earnings: {totalEarnings.toLocaleString('en-US', {
                                            style: 'currency',
                                            currency: 'USD'
                                        })}</h1>
            </div>
            <div className="table-responsive mt-3">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {props.salesList.map((sale)=>{
                        return (
                            <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin} </td>
                            <td>{sale.price.toLocaleString('en-US', {
                                            style: 'currency',
                                            currency: 'USD'
                                        })}</td>
                            <td>
                                <button onClick={() => deleteSale (sale.id, sale)}type="button" className="btn btn-outline-secondary">Delete</button>
                            </td>
                            </tr>
                        )})}
                </tbody>
            </table>
        </div>
        </div>
        </div>
    )
}
export default SalesList;
