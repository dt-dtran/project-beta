
function TechniciansList(props) {

    return (
        <div className="container m-3">
                <h1>Technicians List</h1>
                <div className="table-responsive mt-3">
                <   table className="table table-hover">
                        <thead className="bg-success text-white">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Employee ID</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                props.techniciansList.map(technician => {
                                    return(
                                        <tr key={technician.employee_id}>
                                            <td>{technician.first_name}</td>
                                            <td>{technician.last_name}</td>
                                            <td>{technician.employee_id}</td>
                                        </tr>
                                    )})}
                        </tbody>
                    </table>
            </div>
        </div>
    )
}
export default TechniciansList;
