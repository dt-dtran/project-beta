import React, { useState } from 'react'

function ModelsForm(props) {

    const [ manufacturer, setManufacturer] = useState("");
    const [ name, setName] = useState("");
    const [ picture_url, setPicture_url] = useState("");

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    };

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture_url(value)
    };

    async function handleSubmit (event) {
        event.preventDefault();
        const data = {};
            data.name = name;
            data.picture_url = picture_url;
            data.manufacturer_id = manufacturer

        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            await response.json();

            setManufacturer('');
            setName('');
            setPicture_url('');
            document.getElementById('manufacturer').selectedIndex = 0;
            props.getModelsList()

        } else {
            console.error("Error:", response)
        }
    }

        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
              <h1>Add Vehicle Model</h1>
                <form onSubmit={handleSubmit} id="create-auto" className="create-auto">
                  <div className="form-floating mb-3">
                    <input
                    onChange={handleNameChange}
                    value ={name}
                    placeholder="model name"
                    required
                    type="text"
                    name="model"
                    id="model"
                    className="form-control"
                    />
                <label htmlFor="model">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                    <input
                    onChange={handlePictureChange}
                    value ={picture_url}
                    placeholder="Picture"
                    required
                    type="text"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                    />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
                <div className="form-floating mb-3">
                    <select onChange={handleManufacturerChange} name="manufacturer" id="manufacturer" className="form-select">
                        <option value="">Choose a manufacturer</option>
                        {
                            props.manufacturersList.map((manufacturer) => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                            })
                        }
                    </select>
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>

        )
    }
export default ModelsForm;
