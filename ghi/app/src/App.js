import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from "./ListManufacturers";
import ModelsList from "./ListModels";
import AutosList from "./ListAutos";
import ManufacturerForm from './ManufacturerForm';
import ModelsForm from './ModelsForm';
import AutosForm from './AutosForm';

import CustomersForm from './CustomersForm';
import CustomersList from './CustomersList';
import SalespeopleForm from './SalespeopleForm';
import SalespeopleList from './SalespeopleList';
import SalespeopleHistory from './SalespeopleHistory';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import AppointmentsForm from './AppointmentsForm';
import ListAppointments from './AppointmentsList';
import AppointmentsHistory from './AppointmentsHistory';
import TechniciansForm from './TechniciansForm';
import TechniciansList from './TechniciansList';


function App(props) {

  const [ manufacturersList, setManufacturersList] = useState([]);
  async function getManufacturersList() {

    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      setManufacturersList(data.manufacturers)
    } else {console.error(response)}
  }

  const [ modelsList,  setModelsList] = useState([]);
  async function getModelsList() {

    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      setModelsList(data.models)
    } else {console.error(response)}
  }

  const [ autosList, setAutosList] = useState([]);
  async function getAutosList() {

    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      setAutosList(data.autos)
    } else {console.error(response)}
  }

  const [ customersList, setCustomersList ] = useState([])
  async function getCustomersList() {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomersList(data.customers)}
      else {console.error(response)}
    }

  const [ salespeopleList, setSalespeopleList ] = useState([])
  async function getSalespeopleList() {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeopleList(data.salespeople)}
      else {console.error(response)}
    }

  const [ salesList, setSalesList ] = useState([])
  async function getSalesList() {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesList(data.sales)}
      else {console.error(response)}
  }

  const [ appointmentsList, setAppointmentsList ] = useState([])
  async function getAppointmentList() {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointmentsList(data.appointments);
    } else {console.error(response)}
  }

  const [ techniciansList, setTechniciansList ] = useState([])
  async function getTechniciansList() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechniciansList(data.technicians);
    } else {console.error(response)}
  }

  useEffect(() => {
    getManufacturersList();
    getModelsList();
    getAutosList();
    getTechniciansList();
    getAppointmentList();
    getCustomersList();
    getSalespeopleList();
    getSalesList();
  }, []);


  return (
    <BrowserRouter>
      <Nav/>
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage/>} />
            <Route path="manufacturers">
              <Route path="/manufacturers/list/" element={<ManufacturersList manufacturersList={manufacturersList} setManufacturersList={setManufacturersList} getModelsList={getModelsList} getAutosList={getAutosList} getManufacturersList={getManufacturersList}/> }/>
              <Route path="/manufacturers/new/" element={<ManufacturerForm getManufacturersList={getManufacturersList}/> }/>
              </Route>
            <Route path="models">
              <Route path="/models/list/" element={<ModelsList modelsList={modelsList} setModelsList={setModelsList} getModelsList={getModelsList} getAutosList={getAutosList}/> }/>
              <Route path="/models/new/" element={<ModelsForm manufacturersList={manufacturersList} getModelsList={getModelsList} /> }/>
              </Route>
            <Route path="autos">
              <Route path="/autos/list/" element={<AutosList autosList={autosList} setAutosList={setAutosList} getAutosList={getAutosList}/> }/>
              <Route path="/autos/new/" element={<AutosForm modelsList={modelsList} getAutosList={getAutosList} /> }/>
            </Route>
            <Route path ="salespeople">
              <Route path="/salespeople/list/" element={<SalespeopleList salespeopleList={salespeopleList} setSalespeopleList={setSalespeopleList} />} />
              <Route path="/salespeople/new/" element={<SalespeopleForm getSalespeopleList={getSalespeopleList} />} />
              <Route path="/salespeople/history/" element={<SalespeopleHistory salesList={salesList} salespeopleList={salespeopleList} />} />
            </Route>
            <Route path ="customers">
              <Route path="/customers/list/" element={<CustomersList customersList={customersList} setCustomersList={setCustomersList} />} />
              <Route path="/customers/new/" element={<CustomersForm getCustomersList={getCustomersList}/>} />
            </Route>
            <Route path ="sales">
              <Route path="/sales/list/" element={<SalesList salesList={salesList} setSalesList={setSalesList} getSalesList={getSalesList} getAutosList={getAutosList}/>} />
              <Route path="/sales/new/" element={<SalesForm salespeopleList={salespeopleList} customersList={customersList} autosList={autosList} setAutosList={setAutosList} getAutosList={getAutosList}  getSalesList={getSalesList}/>} />
            </Route>
            <Route path ="technicians">
              <Route path="/technicians/list/" element={<TechniciansList techniciansList={techniciansList} setTechniciansList={setTechniciansList}/>} />
              <Route path="/technicians/new/" element={<TechniciansForm getTechniciansList={getTechniciansList}/>} />
            </Route>
            <Route path ="appointments">
              <Route path="/appointments/list/" element={<ListAppointments appointmentsList={appointmentsList} setAppointmentsList={setAppointmentsList} autosList={autosList} getAppointmentList={getAppointmentList}/>} />
              <Route path="/appointments/new/" element={<AppointmentsForm techniciansList={techniciansList} setTechniciansList={setTechniciansList} getAppointmentList={getAppointmentList}/>} />
              <Route path="/appointments/history/" element={<AppointmentsHistory techniciansList={techniciansList} setTechniciansList={setTechniciansList} appointmentsList={appointmentsList} autosList={autosList} getAppointmentList={getAppointmentList}/>} />
            </Route>
          </Routes>
        </div>
    </BrowserRouter>
  )
}
export default App;
