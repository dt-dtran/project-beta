
function SalespeopleList(props) {

    async function deleteSaleperson(employee_id) {
        const url = `http://localhost:8090/api/salespeople/${employee_id}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (response.ok) {
          props.setSalespeopleList(newList => newList.filter(salesperson=> salesperson.employee_id !== employee_id));
        } else {
          console.error("Cannot delete sale", response);
        }
      }
    return (
        <div className="container m-3">
        <div className="row">
            <h1>Salespeople</h1>
        </div>
        <div className="table-responsive mt-3 ">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {props.salespeopleList.map((salesperson)=> {
                        return (
                            <tr key={salesperson.employee_id}>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td>{salesperson.employee_id}</td>
                                <td>
                                    <button onClick={() => deleteSaleperson(salesperson.employee_id)} type="button" className="btn btn-outline-secondary">Delete</button>
                                </td>
                            </tr>
                        )})}
                </tbody>
            </table>
        </div>
        </div>
    )
}
export default SalespeopleList;
